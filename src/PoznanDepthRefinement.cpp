//  Original authors:
//
//  Adrian Dziembowski,  adrian.dziembowski@put.poznan.pl
//  Dawid Mieloch,       dawid.mieloch@put.poznan.pl
//
//  Pozna� University of Technology, Pozna�, Poland

#include <iostream>
#include "pdrParams.h"
#include "pdrYUV.h"
#include "pdrConfig.h"
#include "pdrRefiner.h"

int main(Int argc, Char *argv[]) {

	if (argc < 2) {
		std::cout << "Use .cfg file\n";
#if _MSC_VER
		system("PAUSE");
#endif
		return EXIT_FAILURE;
	}

	Config *cfg = new Config;
	cfg->readCfgFile(argv[1]);

	cArray<YUV*> inputViews;
	for (UInt i = 0;i < cfg->m_uiNumberOfInputViews;i++) {
		YUV *view = new YUV(cfg->m_cfgInputs[i]);
		inputViews.push_back(view);
	}

	cArray<YUV*> outputViews;
	for (UInt o = 0;o < cfg->m_uiNumberOfOutputViews;o++) {
		YUV* view = new YUV(cfg->m_cfgOutputs[o]);
		outputViews.push_back(view);
	}

	Refiner *ref = new Refiner(inputViews, outputViews, cfg);

	for (UInt frame = cfg->m_uiStartFrame;frame < cfg->m_uiNumberOfFrames + cfg->m_uiStartFrame;frame++) {
		std::cout << "\nFRAME " << frame << "\n";

		ref->loadInputDepthMaps(frame);\
		ref->clearProcessingArrays();
		ref->cvtInputArraysToProcessing();

		ref->refine(frame); // perform depth refinement
		ref->reinitDepth();
		ref->refine(frame); // perform second depth refinement

		ref->saveOutputDepthMaps(frame != cfg->m_uiStartFrame);
	}

	return EXIT_SUCCESS;
}
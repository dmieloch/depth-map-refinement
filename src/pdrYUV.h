#ifndef __PDR_YUV_H__
#define __PDR_YUV_H__

#include "TypeDef.h"
#include "pdrCamParams.h"
#include <string.h>
#include <iostream>

struct CfgYUVParams {
	Bool m_bDepthFileWritingEnabled;

	cArray<String> m_asCameraNames;
	String m_sCurrentCameraName;

	String m_sDepthFilename;

	cArray<CamParams*> m_aCamParams;
	CamParams *m_currentCamParams;

	Int m_iWidth;
	Int m_iHeight;

	UInt m_uiFormat;

	Double m_dAovL;
	Double m_dAovR;
	Double m_dAovT;
	Double m_dAovB;

	Float m_dZNear;
	Float m_dZFar;

	UInt m_uiDepthChromaSubsampling;
	UInt m_uiDepthBitsPerSample;
};

class YUV {
public:
	Bool m_bDepthFileWritingEnabled;

	cArray<String> m_asCameraNames;
	String m_sCurrentCameraName;

	String m_sDepthFilename;

	cArray<CamParams*> m_aCamParams;
	CamParams *m_currentCamParams;

	Int m_iWidth;
	Int m_iHeight;

	UInt m_uiFormat;

	Double m_dAovL;
	Double m_dAovR;
	Double m_dAovT;
	Double m_dAovB;
	
	Double m_dAovW;
	Double m_dAovH;
	
	UInt m_uiDepthChromaWidth;
	UInt m_uiDepthChromaHeight;	
	UInt m_uiDepthChromaSubsampling;

	UInt m_uiDepthLumaFrameSizeInPixels;
	UInt m_uiDepthLumaFrameSizeInBytes;	
	UInt m_uiDepthChromaFrameSizeInPixels;
	UInt m_uiDepthChromaFrameSizeInBytes;

	UInt m_uiDepthBitsPerSample;
	UInt m_uiDepthBytesPerSample;

	UInt m_uiMaxDepthValue;
	Float m_dInvMaxDepthValue;

	Float m_dZNear;
	Float m_dZFar;

	Float m_dInvZFar;
	Float m_dInvZNear;
	Float m_dInvZNearMinusInvZFar;
	Float m_dInvIZNMIZF;

	UChar *m_acD;
	UShort *m_asD;
	Int *m_aiD;
	Float *m_afD;

	~YUV();
	YUV();
	YUV(UInt w, UInt h);
	YUV(CfgYUVParams params);

	void initFromCfg(CfgYUVParams params);

	void initInOutArrays();
	void initProcessingArrays();

	void enableProcessingArraysOutputting(String outputDepthName = "", Double znear = 0, Double zfar = 0);

	void cvtProcessingArraysToOutput();
	void cvtInputArraysToProcessing();

	void readDepthFrame(UInt frame);
	void writeDepthFrame(Bool append);
};

#endif

#ifndef __PDR_REFINER_H__
#define __PDR_REFINER_H__

#include "TypeDef.h"
#include "pdrYUV.h"
#include "pdrConfig.h"
#include <math.h>

#include <iostream>

#if ESTIMATE_TIME
#include <time.h>
#endif

class Refiner {
public:
	UInt m_uiNumberOfInputViews;
	UInt m_uiNumberOfOutputViews;

	cArray<YUV*> m_yuvInput;
	cArray<YUV*> m_yuvOutput;

	cArray<cArray<YUV*>> m_yuvIntermediate;

	UInt m_uiSynthesisMethod;
	Double m_fDepthBlendingThreshold;

	~Refiner();
	Refiner(cArray<YUV*> yuvIn, cArray<YUV*> yuvOut, Config *cfg);

	void refine(Int frame);

	void reinitDepth();

	void inpaintDisocclusions8(UInt o);
	void inpaintSmallAreas(UInt o);

	void loadInputDepthMaps(UInt frame);
	void saveOutputDepthMaps(Bool append);

	void clearProcessingArrays();

	void cvtInputArraysToProcessing();
	void cvtProcessingArraysToOutput();

	void calcImPosOmni(Int &W, Int &H, Double &wPos, Double &hPos, Double &invTargetZ, Matrix_3x1 *XYZ, Double &IAovL, Double &IAovW, Double &IAovB, Double &IAovH);
	void calcImPosPersp(Double &wPos, Double &hPos, Double &invTargetZ, Matrix_3x1 *XYZ, Matrix_4x4 *mat);
	void calcXYZPersp(Int &w, Int &h, Double &invZ, Matrix_3x1 *XYZ, Matrix_4x4 *mat);
	
	void projectDepth(UInt i, UInt o, UInt inFormat, UInt outFormat);

	void filterDepth(UInt i, UInt o);

	void CE5_DepthRefinement(UInt i);
	
	template <typename T>
	void qSort(T tab[], Int left, Int right);
};

#endif

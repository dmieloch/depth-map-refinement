#include "pdrYUV.h"

YUV::~YUV() {

	if (m_acD) { delete m_acD; m_acD = NULL; m_asD = NULL; }

	return;

}

YUV::YUV() {

	m_bDepthFileWritingEnabled = false;

	m_acD = NULL;
	m_asD = NULL;

	return;
}

YUV::YUV(UInt w, UInt h) {

	YUV();

	m_iWidth = w;
	m_iHeight = h;
	m_uiDepthLumaFrameSizeInPixels = m_iWidth * m_iHeight;

	initProcessingArrays();

	return;
}

void YUV::enableProcessingArraysOutputting(String outputDepthName, Double znear, Double zfar) {

	m_bDepthFileWritingEnabled = true;

	m_dZNear = znear;
	m_dZFar = zfar;

	m_uiDepthChromaSubsampling = 420;
	m_uiDepthBitsPerSample = 10;
	m_uiDepthBytesPerSample = 2;

	m_uiMaxDepthValue = (1 << (m_uiDepthBitsPerSample)) - 1;

	m_dInvMaxDepthValue = 1.0 / m_uiMaxDepthValue;

	m_dInvZNear = 1.0 / m_dZNear;
	m_dInvZFar = 1.0 / m_dZFar;
	m_dInvZNearMinusInvZFar = m_dInvZNear - m_dInvZFar;
	m_dInvIZNMIZF = 1.0 / m_dInvZNearMinusInvZFar;

	m_sDepthFilename = outputDepthName;

	if (m_uiDepthChromaSubsampling == 400) {
		m_uiDepthChromaWidth = 0;
		m_uiDepthChromaHeight = 0;
	}		
	else if (m_uiDepthChromaSubsampling == 420) {
		m_uiDepthChromaWidth = m_iWidth / 2;
		m_uiDepthChromaHeight = m_iHeight / 2;
	}		
	else if (m_uiDepthChromaSubsampling == 444) {
		m_uiDepthChromaWidth = m_iWidth;
		m_uiDepthChromaHeight = m_iHeight;
	}

	m_uiDepthChromaFrameSizeInPixels = m_uiDepthChromaWidth * m_uiDepthChromaHeight;
	m_uiDepthLumaFrameSizeInBytes = m_uiDepthLumaFrameSizeInPixels * m_uiDepthBytesPerSample;
	m_uiDepthChromaFrameSizeInBytes = m_uiDepthChromaFrameSizeInPixels * m_uiDepthBytesPerSample;

	initInOutArrays();

	return;
}

void YUV::initFromCfg(CfgYUVParams params) {

	m_bDepthFileWritingEnabled = params.m_bDepthFileWritingEnabled;

	m_sCurrentCameraName = params.m_sCurrentCameraName;
	m_asCameraNames = params.m_asCameraNames;

	m_sDepthFilename = params.m_sDepthFilename;

	m_currentCamParams = params.m_currentCamParams;
	m_aCamParams = params.m_aCamParams;

	m_acD = NULL;
	m_asD = NULL;

	m_iWidth = params.m_iWidth;
	m_iHeight = params.m_iHeight;

	m_uiFormat = params.m_uiFormat;

	m_dAovL = params.m_dAovL;
	m_dAovR = params.m_dAovR;
	m_dAovT = params.m_dAovT;
	m_dAovB = params.m_dAovB;

	m_dAovW = m_dAovR - m_dAovL;
	m_dAovH = m_dAovT - m_dAovB;
	
	m_uiDepthBitsPerSample = params.m_uiDepthBitsPerSample;
	m_uiDepthBytesPerSample = (m_uiDepthBitsPerSample <= 8) ? 1 : 2;

	m_uiDepthChromaSubsampling = params.m_uiDepthChromaSubsampling;

	if (m_uiDepthChromaSubsampling == 400) {
		m_uiDepthChromaWidth = 0;
		m_uiDepthChromaHeight = 0;
	}
	else if (m_uiDepthChromaSubsampling == 420) {
		m_uiDepthChromaWidth = m_iWidth >> 1;
		m_uiDepthChromaHeight = m_iHeight >> 1;
	}
	else if (m_uiDepthChromaSubsampling == 444) {
		m_uiDepthChromaWidth = m_iWidth;
		m_uiDepthChromaHeight = m_iHeight;
	}

	m_uiDepthLumaFrameSizeInPixels = m_iWidth * m_iHeight;
	m_uiDepthChromaFrameSizeInPixels = m_uiDepthChromaWidth * m_uiDepthChromaHeight;

	m_uiDepthLumaFrameSizeInBytes = m_uiDepthLumaFrameSizeInPixels * m_uiDepthBytesPerSample;
	m_uiDepthChromaFrameSizeInBytes = m_uiDepthChromaFrameSizeInPixels * m_uiDepthBytesPerSample;

	m_uiMaxDepthValue = (1 << m_uiDepthBitsPerSample) - 1;

	m_dZNear = params.m_dZNear;
	m_dZFar = params.m_dZFar;

	m_dInvMaxDepthValue = 1.0 / m_uiMaxDepthValue;

	m_dInvZNear = 1.0 / m_dZNear;
	m_dInvZFar = 1.0 / m_dZFar;
	m_dInvZNearMinusInvZFar = m_dInvZNear - m_dInvZFar;
	m_dInvIZNMIZF = 1.0 / m_dInvZNearMinusInvZFar;

	initInOutArrays();
	initProcessingArrays();

	return;
}

YUV::YUV(CfgYUVParams params) {

	initFromCfg(params);
	return;
}

void YUV::initInOutArrays() {

	m_acD = new UChar[m_uiDepthLumaFrameSizeInBytes];
	m_asD = (UShort*)m_acD;

	return;
}

void YUV::initProcessingArrays() {

	m_aiD = new Int[m_uiDepthLumaFrameSizeInPixels];
	m_afD = (Float*)m_aiD;

	return;
}

void YUV::cvtInputArraysToProcessing() {
	
	memset(m_afD, 0, m_uiDepthLumaFrameSizeInPixels);

	if (m_uiDepthBitsPerSample <= 8) {
		for (UInt pp = 0;pp < m_uiDepthLumaFrameSizeInPixels;pp++) {
			m_afD[pp] = m_dInvMaxDepthValue * m_acD[pp] * m_dInvZNearMinusInvZFar + m_dInvZFar;
		}
	}//8bps
	else {
		for (UInt pp = 0;pp < m_uiDepthLumaFrameSizeInPixels;pp++) {
			m_afD[pp] = m_dInvMaxDepthValue * m_asD[pp] * m_dInvZNearMinusInvZFar + m_dInvZFar;
		}
	}//16bps

	return;
}

void YUV::cvtProcessingArraysToOutput() {
	
	memset(m_acD, 0, m_uiDepthLumaFrameSizeInBytes);
	
	if (m_bDepthFileWritingEnabled) {
		if (m_uiDepthBitsPerSample <= 8) {
			for (UInt pp = 0;pp < m_uiDepthLumaFrameSizeInPixels;pp++) {
				m_acD[pp] = -(m_dInvZFar / m_afD[pp] - 1) * (m_dInvIZNMIZF*m_afD[pp])*m_uiMaxDepthValue;
			}
		}//8bps
		else {
			for (UInt pp = 0;pp < m_uiDepthLumaFrameSizeInPixels;pp++) {
				m_asD[pp] = -(m_dInvZFar / m_afD[pp] - 1) * (m_dInvIZNMIZF*m_afD[pp])*m_uiMaxDepthValue;
			}
		}//16bps
	}

	return;
}

void YUV::readDepthFrame(UInt frame) {

	FILE* fileYUV = NULL;
	fileYUV = fopen(m_sDepthFilename.c_str(), "rb");

	Int64 offset = m_uiDepthLumaFrameSizeInBytes + m_uiDepthChromaFrameSizeInBytes + m_uiDepthChromaFrameSizeInBytes;

  xfseek64(fileYUV, offset*frame, 0);
	fread(m_acD, 1, m_uiDepthLumaFrameSizeInBytes, fileYUV);

	fclose(fileYUV);

	return;
}

void YUV::writeDepthFrame(Bool append) {

	if (!m_bDepthFileWritingEnabled) {
		return;
	}

	FILE* fileYUV = NULL;
	fileYUV = fopen(m_sDepthFilename.c_str(), (append) ? "ab" : "wb");

	if (fileYUV == NULL) {
		fprintf(stdout, "Error: yuv file %s already opened\n", m_sDepthFilename.c_str());
		return;
	}

	fwrite(m_acD, 1, m_uiDepthLumaFrameSizeInBytes, fileYUV);

	if (m_uiDepthChromaSubsampling != 400) {
		if (m_uiDepthBytesPerSample == 1) {
			UChar *tmp = new UChar[m_uiDepthChromaFrameSizeInPixels];
			for (UInt pp = 0;pp < m_uiDepthChromaFrameSizeInPixels;pp++) tmp[pp] = m_uiMaxDepthValue / 2;
			fwrite(tmp, 1, m_uiDepthChromaFrameSizeInBytes, fileYUV);
			fwrite(tmp, 1, m_uiDepthChromaFrameSizeInBytes, fileYUV);
			delete tmp;
		}
		else {
			UShort *tmp = new UShort[m_uiDepthChromaFrameSizeInPixels];
			for (UInt pp = 0;pp < m_uiDepthChromaFrameSizeInPixels;pp++) tmp[pp] = m_uiMaxDepthValue / 2;
			fwrite(tmp, 1, m_uiDepthChromaFrameSizeInBytes, fileYUV);
			fwrite(tmp, 1, m_uiDepthChromaFrameSizeInBytes, fileYUV);
			delete tmp;
		}
	}

	fclose(fileYUV);

	return;
}


